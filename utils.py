import numpy as np
import casadi as ca
import control


def arrayToInput(val):
    inputs = {'Z01_T_Thermostat_sp':[], 'Z02_T_Thermostat_sp':[], 'Bd_Load_CPU':[]}
    
    inputs['Z01_T_Thermostat_sp'].append(val[0])
    inputs['Z02_T_Thermostat_sp'].append(val[1])
    inputs['Bd_Load_CPU'].append(0.5)
    return inputs

def arrayToInputTempFan(val):
    inputs = {'Z01_Fl_Fan_sp':[], 'Z01_T_HVAC_sp':[], 'Z02_Fl_Fan_sp':[], 'Z02_T_HVAC_sp':[], 'Bd_Load_CPU':[]}
    
    inputs['Z01_Fl_Fan_sp'].append(val[0])
    inputs['Z01_T_HVAC_sp'].append(val[1])
    inputs['Z02_Fl_Fan_sp'].append(val[2])
    inputs['Z02_T_HVAC_sp'].append(val[3])
    inputs['Bd_Load_CPU'].append(0.5)
    return inputs

def outputToArray(outputs):
    """return the outputs Z01_T and Z02_T out of the list of collected outputs"""
    vals = [[],[]]
    for output in outputs:
        vals[0].append(output['Z01_T'])
        vals[1].append(output['Z02_T'])
    return np.array(vals)


def LinReg(X, y):
    opti = ca.Opti()
    (N, nx) = X.shape
    a = opti.variable(nx)
    b = opti.variable(1)

    cost = 0
    for i in range(N):
        cost = cost + (ca.dot(a,X[i]) + b - y[i])**2

    opti.minimize(cost)
    opti.solver('ipopt')
    sol = opti.solve()
    print(f"a {sol.value(a)}")
    return sol.value(a), sol.value(b)


class StateObserver():
    def __init__(self, A, B, C, D, y0):
        self.A = A
        self.B = B
        self.C = C
        self.D = D
        self.u_len = self.B.shape[1]
        self.y_len = self.C.shape[0]
        evs = [0.58 + 0.05 * i for i in range(A.shape[0])]
        self.L = control.place(A.T, C.T, evs).T
        print(self.L)
        self.state = np.dot(np.linalg.pinv(self.C), y0).reshape((-1,1))

    def updateState(self, y, u):
        y = y.reshape((self.y_len,1))
        u = u.reshape((self.u_len,1))
        yhat = np.dot(self.C, self.state)
        self.state = np.dot(self.A, self.state) + np.dot(self.L, y-yhat) + np.dot(self.B, u)
    
    def getState(self):
        return self.state


def registerOutput(outDict, out, name):
    for key in outDict[name]:
        outDict[name][key].append(out[key])
    return outDict


class SimpleController(object):
    """ Simple rule-based controller for models with only thermostat setpoints as control variables.
    """

    def __init__(self, control_list, lower_tol, upper_tol, nighttime_setback=False, nighttime_start=17, nighttime_end=6, nighttime_temp=18):
        self.has_uc_input = False
        for control in control_list:
            if not "T_Thermostat_sp" in control:
                if control == 'Bd_Load_CPU':
                    self.has_uc_input = True
                else:
                    raise TypeError(
                        "Only thermostat setpoints are supported by this controller!")
        self.controls = control_list
        if self.has_uc_input:
            self.controls.remove('Bd_Load_CPU')
        self.observations = [control[0:5] for control in self.controls]
        self.tol1 = lower_tol
        self.tol2 = upper_tol
        self.nighttime_setback = nighttime_setback
        self.nighttime_start = nighttime_start
        self.nighttime_end = nighttime_end
        self.nighttime_temp = nighttime_temp

    def get_control(self, obs, temp_sp, hour=None):
        controls = {}
        if self.nighttime_setback:
            if hour < self.nighttime_end or hour > self.nighttime_start:
                for control in self.controls:
                    controls[control] = [self.nighttime_temp]
            else:
                for control in self.controls:
                    controls[control] = [temp_sp]
        else:
            for control in self.controls:
                controls[control] = [temp_sp]
        for measurement in self.observations:
            control_name = measurement + "_Thermostat_sp"
            observation = obs[measurement]
            control_temp = controls[control_name][0]
            if observation - control_temp < self.tol1 and control_temp - observation < self.tol1:
                control_temp = control_temp
            elif observation - control_temp > self.tol1 and observation - control_temp < self.tol2:
                control_temp = control_temp - 0.1
            elif observation - control_temp > self.tol2:
                control_temp = control_temp - 0.5
            elif control_temp - observation > self.tol1 and control_temp - observation < self.tol2:
                control_temp = control_temp + 0.1
            elif control_temp - observation > self.tol2:
                control_temp = control_temp + 0.5
            controls[control_name][0] = control_temp
        if self.has_uc_input:
            controls['Bd_Load_CPU'] = [0.5]
        return controls