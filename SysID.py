from sippy import *
from utils import LinReg, arrayToInput, arrayToInputTempFan, outputToArray
import numpy as np
import random


def sampleDataStateSpace(env, excitationslist, num=50):
    outputs = []
    inputs = []
    tempData = []
    # Varying second sp while fixing first sp
    for i in range(int(num/16)):
        excit = excitationslist[i%4]
        inputArray = [[21, val] for val in excit]
        for i in range(len(inputArray)):
            inp = arrayToInput(inputArray[i])
            output = env.step(inp)
            outputs.append(output)
            tempData.append(output['Ext_T'])
        inputs.extend(inputArray)
    # reset phase
    inputArray = [[21, 21] for i in range(3)]
    for i in range(len(inputArray)):
        inp = arrayToInput(inputArray[i])
        output = env.step(inp)
        outputs.append(output)
        tempData.append(output['Ext_T'])
    inputs.extend(inputArray)
    # Varying first sp while fixing second sp
    for i in range(int(num/16)):
        excit = excitationslist[i%4]
        inputArray = [[val, 21] for val in excit]
        for i in range(len(inputArray)):
            inp = arrayToInput(inputArray[i])
            output = env.step(inp)
            outputs.append(output)
            tempData.append(output['Ext_T'])
        inputs.extend(inputArray)
    # reset phase
    inputArray = [[21, 21] for i in range(3)]
    for i in range(len(inputArray)):
        inp = arrayToInput(inputArray[i])
        output = env.step(inp)
        outputs.append(output)
        tempData.append(output['Ext_T'])
    inputs.extend(inputArray)
    # Varying both sps
    for i in range(int(num/4)):
        excit = excitationslist[i%8]
        inputArray = [[val, val] for val in excit]
        for i in range(len(inputArray)):
            inp = arrayToInput(inputArray[i])
            output = env.step(inp)
            outputs.append(output)
            tempData.append(output['Ext_T'])
        inputs.extend(inputArray)
    for i in range(int(num/2)):
        excit = excitationslist[i%4+4]
        inputArray = [[val, val] for val in excit]
        for i in range(len(inputArray)):
            inp = arrayToInput(inputArray[i])
            output = env.step(inp)
            outputs.append(output)
            tempData.append(output['Ext_T'])
        inputs.extend(inputArray)
    for i in range(int(num/8)):
        excit = excitationslist[i%4]
        inputArray = [[val, val] for val in excit]
        for i in range(len(inputArray)):
            inp = arrayToInput(inputArray[i])
            output = env.step(inp)
            outputs.append(output)
            tempData.append(output['Ext_T'])
        inputs.extend(inputArray)
    for i in range(int(num/2)):
        excit = excitationslist[-(i%4)]
        inputArray = [[val, val] for val in excit]
        for i in range(len(inputArray)):
            inp = arrayToInput(inputArray[i])
            output = env.step(inp)
            outputs.append(output)
            tempData.append(output['Ext_T'])
        inputs.extend(inputArray)
    # Varying second sp while fixing first sp
    for i in range(int(num/16)):
        excit = excitationslist[i%3+4]
        inputArray = [[21, val] for val in excit]
        for i in range(len(inputArray)):
            inp = arrayToInput(inputArray[i])
            output = env.step(inp)
            outputs.append(output)
            tempData.append(output['Ext_T'])
        inputs.extend(inputArray)
    # reset phase
    inputArray = [[21, 21] for i in range(3)]
    for i in range(len(inputArray)):
        inp = arrayToInput(inputArray[i])
        output = env.step(inp)
        outputs.append(output)
        tempData.append(output['Ext_T'])
    inputs.extend(inputArray)
    # Varying first sp while fixing second sp
    for i in range(int(num/16)):
        excit = excitationslist[i%3+4]
        inputArray = [[val, 21] for val in excit]
        for i in range(len(inputArray)):
            inp = arrayToInput(inputArray[i])
            output = env.step(inp)
            outputs.append(output)
            tempData.append(output['Ext_T'])
        inputs.extend(inputArray)
    outArray = outputToArray(outputs)

    # To incorporate temperature data in the inputs
    inputs[0].append(tempData[0])
    for i in range(len(inputs)-1):
        inputs[i+1].append(tempData[i])
        
    return np.array(inputs), np.array(outArray)


def sampleDataStateSpaceTempFan(env, excitationslistFan, excitationslistTemp, num=50):
    outputs = []
    inputs = []
    tempData = []
    # Varying sps on intervals
    random.seed(1)
    for i in range(num):
        Z01_Fan_sps = random.sample(excitationslistFan, 1)[0]
        Z01_Temp_sps = random.sample(excitationslistTemp, 1)[0]
        Z02_Fan_sps = random.sample(excitationslistFan, 1)[0]
        Z02_Temp_sps = random.sample(excitationslistTemp, 1)[0]
        inputArray = [[Z01_Fan_sps[j], Z01_Temp_sps[j], Z02_Fan_sps[j], Z02_Temp_sps[j]] for j in range(len(Z01_Fan_sps))]
        for k in range(len(inputArray)):
            inp = arrayToInputTempFan(inputArray[k])
            output = env.step(inp)
            outputs.append(output)
            tempData.append(output['Ext_T'])
        inputs.extend(inputArray)
    outArray = outputToArray(outputs)

    # To incorporate temperature data in the inputs
    inputs[0].append(tempData[0])
    for i in range(len(inputs)-1):
        inputs[i+1].append(tempData[i])
        
    return np.array(inputs), np.array(outArray)


def sampleDataLinReg(env):
    outputs = []
    inputs = []
    tempData = []
    pwData = []
    random.seed(1)
    # Varying both sps separately
    for i in range(500):
        val1 = random.uniform(17,23)
        val2 = random.uniform(17,23)
        inputArray = [[val1, val2], [val1, val2], [val1,val2]]
        for i in range(3):
            inp = arrayToInput(inputArray[i])
            output = env.step(inp)
            outputs.append(output)
            tempData.append(output['Ext_T'])
            pwData.append(output['Fa_Pw_All']/1000) 
        inputs.extend(inputArray)
    outArray = outputToArray(outputs)

    # To incorporate temperature data
    inputs[0].append(tempData[0])
    for i in range(len(inputs)-1):
        inputs[i+1].append(tempData[i])
        
    return np.array(inputs), np.array(outArray), np.array(pwData)


def sampleDataLinRegTempFan(env):
    outputs = []
    inputs = []
    tempData = []
    pwData = []
    random.seed(1)
    # Varying the sps separately
    for i in range(500):
        val1 = random.uniform(1.75,7)
        val2 = random.uniform(13,21)
        val3 = random.uniform(1.75,7)
        val4 = random.uniform(13,21)
        inputArray = [[val1, val2, val3, val4], [val1, val2, val3, val4], [val1, val2, val3, val4]]
        for i in range(3):
            inp = arrayToInputTempFan(inputArray[i])
            output = env.step(inp)
            outputs.append(output)
            tempData.append(output['Ext_T'])
            pwData.append(output['Fa_Pw_All']/1000)
        inputs.extend(inputArray)
    outArray = outputToArray(outputs)

    # To incorporate temperature data
    inputs[0].append(tempData[0])
    for i in range(len(inputs)-1):
        inputs[i+1].append(tempData[i])
        
    return np.array(inputs), np.array(outArray), np.array(pwData)
    

def combineMatrices(sys1, sys2):
    A = np.block([[sys1.A, np.zeros_like(sys1.A)],
                [np.zeros_like(sys1.A), sys2.A]])
    B = np.block([[sys1.B],
                [sys2.B]])
    C = np.block([[sys1.C, np.zeros_like(sys1.C)],
                [np.zeros_like(sys1.C), sys2.C]])
    D = np.block([[sys1.D],
                [sys2.D]])
    return A, B, C, D


def sysID(env, excitationslist, num=100, idType='krr'):
    if idType == 'ss':
        inputs, tempOutputs = sampleDataStateSpace(env, excitationslist, num=num)
        idsystem1 = system_identification(np.array([tempOutputs[0]]), inputs, 'N4SID', SS_fixed_order=2)#
        idsystem2 = system_identification(np.array([tempOutputs[1]]), inputs, 'N4SID', SS_fixed_order=2)
        A, B, C, D = combineMatrices(idsystem1, idsystem2)
        return A, B, C, D
    elif idType == 'lin':
        inputs, tempOutputs, pwOutputs = sampleDataLinReg(envt)
        X = []
        for i in range(0, len(inputs)-1):
            X.append([*tempOutputs.T[i], *inputs[i+1]])
        X = np.array(X)
        a, b = LinReg(X,pwOutputs[1:])
        return a, b
    else:
        raise TypeError(f"Unknown idType {idType}")


def sysIDTempFan(env, excitationslistFan, excitationslistTemp, num=100, idType='krr'):
    if idType == 'ss':
        inputs, tempOutputs = sampleDataStateSpaceTempFan(env, excitationslistFan, excitationslistTemp, num=num)
        idsystem1 = system_identification(np.array([tempOutputs[0]]), inputs, 'N4SID', SS_fixed_order=2)#
        idsystem2 = system_identification(np.array([tempOutputs[1]]), inputs, 'N4SID', SS_fixed_order=2)
        A, B, C, D = combineMatrices(idsystem1, idsystem2)
        return A, B, C, D
    elif idType == 'lin':
        inputs, tempOutputs, pwOutputs = sampleDataLinRegTempFan(env)
        X = []
        for i in range(0, len(inputs)-1):
            X.append([*tempOutputs.T[i], *inputs[i+1]])
        X = np.array(X)
        a, b = LinReg(X,pwOutputs[1:])
        return a, b
    else:
        raise TypeError(f"Unknown idType {idType}")