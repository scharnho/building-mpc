import numpy as np
import casadi as ca


class OCP():
    def __init__(self, A, B, C, D, l_y, u_y, l_u, u_u, f=None, a=None, b=None):
        self.A = A
        self.B = B
        self.C = C
        self.D = D
        self.l_y = l_y
        self.u_y = u_y
        self.l_u = l_u
        self.u_u = u_u
        self.f = f
        self.a = a
        self.b = b

    def solve(self, x0, p0, ybar, N, Q, T, w=1, probType='spTrack'):
        opti = ca.Opti()
        x = opti.variable(len(self.A[0]), N)
        u = opti.variable(3, N-1)
        eps = 0
        eps_vec = np.array([eps, eps])

        cost = 0
        
        opti.subject_to(x[:, 0] == x0)

        if probType == 'spTrack':
            for i in range(N):
                arg = ca.fmax(
                    ca.fabs(ca.mtimes(self.C, x[:, i]) - ybar) - eps_vec, np.zeros((2, 1)))
                cost = cost + w * ca.bilin(Q, arg, arg)
            for i in range(N-1):
                opti.subject_to(x[:, i+1] == ca.mtimes(self.A,
                                                       x[:, i]) + ca.mtimes(self.B, u[:, i]))
                opti.subject_to(opti.bounded(
                    self.l_y, ca.mtimes(self.C, x[:, i])[0], self.u_y))
                opti.subject_to(opti.bounded(
                    self.l_y, ca.mtimes(self.C, x[:, i])[1], self.u_y))
                opti.subject_to(opti.bounded(self.l_u, u[0, i], self.u_u))
                opti.subject_to(opti.bounded(self.l_u, u[1, i], self.u_u))
                opti.subject_to(u[2, i] == T[i])
                opti.set_initial(u[0, i], ybar[0])
                opti.set_initial(u[1, i], ybar[1])
            opti.subject_to(opti.bounded(
                    self.l_y, ca.mtimes(self.C, x[:, N-1])[0], self.u_y))
            opti.subject_to(opti.bounded(
                    self.l_y, ca.mtimes(self.C, x[:, N-1])[1], self.u_y))

        elif probType == 'linPW':
            pw = opti.variable(1, N)
            inp = opti.variable(5, N)
            opti.subject_to(pw[:, 0] == p0)
            for i in range(N):
                arg = ca.fmax(
                    ca.fabs(ca.mtimes(self.C, x[:, i]) - ybar) - eps_vec, np.zeros((2, 1)))
                cost = cost + w * ca.bilin(Q, arg, arg)
            for i in range(N-1):
                cost = cost + (1 - w) * pw[:, i]
                opti.subject_to(inp[:2, i] == ca.mtimes(self.C, x[:, i]))
                opti.subject_to(inp[2:, i] == u[:, i])
                opti.subject_to(pw[:, i+1] == ca.dot(self.a, inp[:, i]) + self.b)
            for i in range(N-1):
                opti.subject_to(x[:, i+1] == ca.mtimes(self.A,
                                                       x[:, i]) + ca.mtimes(self.B, u[:, i]))
                opti.subject_to(opti.bounded(
                    self.l_y, ca.mtimes(self.C, x[:, i])[0], self.u_y))
                opti.subject_to(opti.bounded(
                    self.l_y, ca.mtimes(self.C, x[:, i])[1], self.u_y))
                opti.subject_to(opti.bounded(self.l_u, u[0, i], self.u_u))
                opti.subject_to(opti.bounded(self.l_u, u[1, i], self.u_u))
                opti.subject_to(u[2, i] == T[i])
                opti.set_initial(u[0, i], ybar[0])
                opti.set_initial(u[1, i], ybar[1])
            opti.subject_to(opti.bounded(
                    self.l_y, ca.mtimes(self.C, x[:, N-1])[0], self.u_y))
            opti.subject_to(opti.bounded(
                    self.l_y, ca.mtimes(self.C, x[:, N-1])[1], self.u_y))

        elif probType == 'krrPW':
            pw = opti.variable(1, N)
            inp = opti.variable(5, N)
            opti.subject_to(pw[:, 0] == p0)
            for i in range(N):
                arg = ca.fmax(
                    ca.fabs(ca.mtimes(self.C, x[:, i]) - ybar) - eps_vec, np.zeros((2, 1)))
                cost = cost + w * ca.bilin(Q, arg, arg)
            for i in range(N-1):
                cost = cost + (1 - w) * pw[:, i]
                opti.subject_to(inp[:2, i] == ca.mtimes(self.C, x[:, i]))
                opti.subject_to(inp[2:, i] == u[:, i])
                opti.subject_to(pw[:, i+1] == self.f.eval([inp[:, i]]))
            for i in range(N-1):
                opti.subject_to(x[:, i+1] == ca.mtimes(self.A,
                                                       x[:, i]) + ca.mtimes(self.B, u[:, i]))
                opti.subject_to(opti.bounded(
                    self.l_y, ca.mtimes(self.C, x[:, i])[0], self.u_y))
                opti.subject_to(opti.bounded(
                    self.l_y, ca.mtimes(self.C, x[:, i])[1], self.u_y))
                opti.subject_to(opti.bounded(self.l_u, u[0, i], self.u_u))
                opti.subject_to(opti.bounded(self.l_u, u[1, i], self.u_u))
                opti.subject_to(u[2, i] == T[i])
                opti.set_initial(u[0, i], ybar[0])
                opti.set_initial(u[1, i], ybar[1])
            opti.subject_to(opti.bounded(
                    self.l_y, ca.mtimes(self.C, x[:, N-1])[0], self.u_y))
            opti.subject_to(opti.bounded(
                    self.l_y, ca.mtimes(self.C, x[:, N-1])[1], self.u_y))

        elif probType == 'hardPW':
            pw = opti.variable(1, N)
            inp = opti.variable(5, N)
            opti.subject_to(pw[:, 0] == p0)
            for i in range(N-1):
                cost = cost + pw[:, i]
                opti.subject_to(inp[:2, i] == ca.mtimes(self.C, x[:, i]))
                opti.subject_to(inp[2:, i] == u[:, i])
                opti.subject_to(pw[:, i+1] == ca.dot(self.a, inp[:, i]) + self.b)
            for i in range(N-1):
                opti.subject_to(x[:, i+1] == ca.mtimes(self.A,
                                                       x[:, i]) + ca.mtimes(self.B, u[:, i]))
                opti.subject_to(opti.bounded(
                    self.l_y, ca.mtimes(self.C, x[:, i])[0], self.u_y))
                opti.subject_to(opti.bounded(
                    self.l_y, ca.mtimes(self.C, x[:, i])[1], self.u_y))
                opti.subject_to(opti.bounded(self.l_u, u[0, i], self.u_u))
                opti.subject_to(opti.bounded(self.l_u, u[1, i], self.u_u))
                opti.subject_to(u[2, i] == T[i])
                opti.set_initial(u[0, i], ybar[0])
                opti.set_initial(u[1, i], ybar[1])
            opti.subject_to(opti.bounded(
                    self.l_y, ca.mtimes(self.C, x[:, N-1])[0], self.u_y))
            opti.subject_to(opti.bounded(
                    self.l_y, ca.mtimes(self.C, x[:, N-1])[1], self.u_y))

        elif probType == 'softPW':
            pw = opti.variable(1, N)
            inp = opti.variable(5, N)
            s = opti.variable(len(self.A[0]), N)
            opti.subject_to(pw[:, 0] == p0)
            for i in range(N-1):
                cost = cost +  pw[:, i]
                opti.subject_to(inp[:2, i] == ca.mtimes(self.C, x[:, i]))
                opti.subject_to(inp[2:, i] == u[:, i])
                opti.subject_to(pw[:, i+1] == ca.dot(self.a, inp[:, i]) + self.b)
                cost = cost + 5 * s[0,i] + 5 * s[1,i]
            cost = cost + 5 * s[0,N-1] + 5 * s[1,N-1]
            for i in range(N-1):
                opti.subject_to(x[:, i+1] == ca.mtimes(self.A,
                                                       x[:, i]) + ca.mtimes(self.B, u[:, i]))
                opti.subject_to(opti.bounded(
                    self.l_y-s[0,i], ca.mtimes(self.C, x[:, i])[0], self.u_y+s[0,i]))
                opti.subject_to(opti.bounded(
                    self.l_y-s[1,i], ca.mtimes(self.C, x[:, i])[1], self.u_y+s[1,i]))
                opti.subject_to(opti.bounded(self.l_u, u[0, i], self.u_u))
                opti.subject_to(opti.bounded(self.l_u, u[1, i], self.u_u))
                opti.subject_to(s[0, i] >= 0)
                opti.subject_to(s[1, i] >= 0)
                opti.subject_to(u[2, i] == T[i])
                opti.set_initial(u[0, i], ybar[0])
                opti.set_initial(u[1, i], ybar[1])
            opti.subject_to(opti.bounded(
                    self.l_y-s[0,N-1], ca.mtimes(self.C, x[:, N-1])[0], self.u_y+s[0,N-1]))
            opti.subject_to(opti.bounded(
                    self.l_y-s[1,N-1], ca.mtimes(self.C, x[:, N-1])[1], self.u_y+s[1,N-1]))
            opti.subject_to(s[0, N-1] >= 0)
            opti.subject_to(s[1, N-1] >= 0)

        opti.minimize(cost)
        opti.solver('ipopt')
        sol = opti.solve()
        if N-1 == 1:
            ures = sol.value(u).T
        else:
            ures = sol.value(u).T[0]
        return ures 



class OCPTempFan():
    def __init__(self, A, B, C, D, l_y, u_y, l_fan, u_fan, l_temp, u_temp, f=None, a=None, b=None):
        self.A = A
        self.B = B
        self.C = C
        self.D = D
        self.l_y = l_y
        self.u_y = u_y
        self.l_fan = l_fan
        self.u_fan = u_fan
        self.l_temp = l_temp
        self.u_temp = u_temp
        self.f = f
        self.a = a
        self.b = b

    def solve(self, x0, p0, ybar, N, Q, T, w=1, probType='spTrack'):
        opti = ca.Opti()
        x = opti.variable(len(self.A[0]), N)
        u = opti.variable(5, N-1)
        eps = 0
        eps_vec = np.array([eps, eps])

        cost = 0
        
        opti.subject_to(x[:, 0] == x0)

        if probType == 'spTrack':
            for i in range(N):
                arg = ca.fmax(
                    ca.fabs(ca.mtimes(self.C, x[:, i]) - ybar) - eps_vec, np.zeros((2, 1)))
                cost = cost + w * ca.bilin(Q, arg, arg)
            for i in range(N-1):
                opti.subject_to(x[:, i+1] == ca.mtimes(self.A,
                                                       x[:, i]) + ca.mtimes(self.B, u[:, i]))
                opti.subject_to(opti.bounded(
                    self.l_y, ca.mtimes(self.C, x[:, i])[0], self.u_y))
                opti.subject_to(opti.bounded(
                    self.l_y, ca.mtimes(self.C, x[:, i])[1], self.u_y))
                opti.subject_to(opti.bounded(self.l_fan, u[0, i], self.u_fan))
                opti.subject_to(opti.bounded(self.l_temp, u[1, i], self.u_temp))
                opti.subject_to(opti.bounded(self.l_fan, u[2, i], self.u_fan))
                opti.subject_to(opti.bounded(self.l_temp, u[3, i], self.u_temp))
                opti.subject_to(u[4, i] == T[i])
                opti.set_initial(u[0, i], ybar[0])
                opti.set_initial(u[2, i], ybar[1])
                opti.set_initial(u[1, i], 7)
                opti.set_initial(u[3, i], 7)
            opti.subject_to(opti.bounded(
                    self.l_y, ca.mtimes(self.C, x[:, N-1])[0], self.u_y))
            opti.subject_to(opti.bounded(
                    self.l_y, ca.mtimes(self.C, x[:, N-1])[1], self.u_y))

        elif probType == 'linPW':
            pw = opti.variable(1, N)
            inp = opti.variable(7, N)
            opti.subject_to(pw[:, 0] == p0)
            for i in range(N):
                arg = ca.fmax(
                    ca.fabs(ca.mtimes(self.C, x[:, i]) - ybar) - eps_vec, np.zeros((2, 1)))
                cost = cost + w * ca.bilin(Q, arg, arg)
            for i in range(N-1):
                cost = cost + (1 - w) * pw[:, i]
                opti.subject_to(inp[:2, i] == ca.mtimes(self.C, x[:, i]))
                opti.subject_to(inp[2:, i] == u[:, i])
                opti.subject_to(pw[:, i+1] == ca.dot(self.a, inp[:, i]) + self.b)
            for i in range(N-1):
                opti.subject_to(x[:, i+1] == ca.mtimes(self.A,
                                                       x[:, i]) + ca.mtimes(self.B, u[:, i]))
                opti.subject_to(opti.bounded(
                    self.l_y, ca.mtimes(self.C, x[:, i])[0], self.u_y))
                opti.subject_to(opti.bounded(
                    self.l_y, ca.mtimes(self.C, x[:, i])[1], self.u_y))
                opti.subject_to(opti.bounded(self.l_fan, u[0, i], self.u_fan))
                opti.subject_to(opti.bounded(self.l_temp, u[1, i], self.u_temp))
                opti.subject_to(opti.bounded(self.l_fan, u[2, i], self.u_fan))
                opti.subject_to(opti.bounded(self.l_temp, u[3, i], self.u_temp))
                opti.subject_to(u[4, i] == T[i])
                opti.set_initial(u[0, i], ybar[0]-2)
                opti.set_initial(u[2, i], ybar[1]-2)
                opti.set_initial(u[1, i], 7)
                opti.set_initial(u[3, i], 7)
            opti.subject_to(opti.bounded(
                    self.l_y, ca.mtimes(self.C, x[:, N-1])[0], self.u_y))
            opti.subject_to(opti.bounded(
                    self.l_y, ca.mtimes(self.C, x[:, N-1])[1], self.u_y))

        opti.minimize(cost)
        opti.solver('ipopt') 
        sol = opti.solve()
        if N-1 == 1:
            ures = sol.value(u).T
        else:
            ures = sol.value(u).T[0]
        return ures  