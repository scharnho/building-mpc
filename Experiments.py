import numpy as np
import matplotlib.pyplot as plt
from MPC import OCP, OCPTempFan
from SysID import sysID, sysIDTempFan
from utils import StateObserver,  registerOutput, SimpleController, arrayToInput, outputToArray, arrayToInputTempFan
import best



def initEnv(env, A, B, C, D, sp_temp):
    env.reset()

    luen = StateObserver(A, B, C, D, np.array([sp_temp, sp_temp]))

    # init phase:
    for _ in range(96):
        u = np.array([sp_temp, sp_temp])
        out = env.step(arrayToInput(u))
        y = outputToArray([out])
        u = np.array([sp_temp, sp_temp, out["Ext_T"]])
        luen.updateState(y, u)
    return luen, out

def initEnvTempFan(env, A, B, C, D):
    env.reset()

    luen = StateObserver(A, B, C, D, np.array([23, 23]))

    # init phase:
    for _ in range(96):
        u = np.array([7, 16, 7, 15])
        out = env.step(arrayToInputTempFan(u))
        y = outputToArray([out])
        u = np.array([7, 16, 7, 15, out["Ext_T"]])
        luen.updateState(y, u)
    return luen, out


env = best.make('DatacenterThermostat-v0', simulation_days=100)


#############################################################
# System  identification
#############################################################

increasesp1 = np.linspace(17,20,20)
increasesp2 = np.linspace(20,23,20)
constsp1 = np.linspace(23,23,20)
decreasesp1 = np.linspace(23,20,20)
constsp2 = np.linspace(21,21,20)
decreasesp2 = np.linspace(20,17,20)
constsp3 = np.linspace(19,19,20)
constsp4 = np.linspace(17,17,20)

excitationslist = [increasesp1, increasesp2, decreasesp1, decreasesp2, constsp1, constsp2, constsp3, constsp4]

A, B, C, D = sysID(env, excitationslist, num=100, idType='ss')      # State space model
a, b = sysID(env, excitationslist, idType='lin')                    # linear regression

#############################################################
# Hyperparameters
#############################################################

num_steps = 480                     # simulation timesteps, 5 days
sp_temp = 21                        # Goal temperature
inputs = env.get_inputs()           # list of input names
lower_tol = 0.2                     # lower tolerance in RBC
upper_tol = 1.0                     # upper tolerance in RBC
N_MPC = 10                          # MPC horizopn length
outputCollection = {}               # dict to save all simulation data
kpiDict = {}                        # dict to save the kpis from all simulations
T_lower = 17
T_upper = 24
u_lower = 18
u_upper = 23
u_lower_pw_only = 15
T_lower_pw_only = 19
Q = np.diag(np.array([1,1]))
ws = [0.95, 0.85, 0.75]

#############################################################
# RBC
#############################################################

rbc = SimpleController(inputs, lower_tol, upper_tol)
outputCollection["rbc"] = {"Z01_T":[], "Z02_T":[], "Fa_Pw_All":[], "Fa_Pw_HVAC":[], "Ext_T":[]}
env.reset()
luen, out = initEnv(env, A, B, C, D, sp_temp)

for _ in range(num_steps):
    u = rbc.get_control(out, sp_temp)
    out = env.step(u)
    outputCollection = registerOutput(outputCollection, out, "rbc")

pwSum, pwAvg = env.get_power_kpi(pw_type="All", start_ind=95)
comAvg, totViolations = env.get_comfort_kpi(target=sp_temp, threshold=upper_tol, start_ind=95)
kpiDict["rbc"] = {"pwAvg":pwAvg, "comAvg":comAvg}


#############################################################
# Setpoint tracking
#############################################################

ocp = OCP(A, B, C, D, T_lower, T_upper, u_lower, u_upper)
outputCollection["sp"] = {"Z01_T":[], "Z02_T":[], "Fa_Pw_All":[], "Fa_Pw_HVAC":[], "Ext_T":[]}
env.reset()
luen, out = initEnv(env, A, B, C, D, sp_temp)

for _ in range(num_steps):
    x0 = luen.getState()
    forecast = env.get_forecast(N_MPC)
    u = ocp.solve(x0, out["Fa_Pw_All"], np.array([sp_temp, sp_temp]), N_MPC, Q, forecast["Dry Bulb Temperature"], probType='spTrack')
    out = env.step(arrayToInput(u))
    outputCollection = registerOutput(outputCollection, out, "sp")
    obs = outputToArray([out])
    luen.updateState(obs, u)


pwSum, pwAvg = env.get_power_kpi(pw_type="All", start_ind=95)
comAvg, totViolations = env.get_comfort_kpi(target=sp_temp, threshold=upper_tol, start_ind=95)
kpiDict["sp"] = {"pwAvg":pwAvg, "comAvg":comAvg}


#############################################################
# Mixed objective
#############################################################

for w in ws:
    key = "mix"+str(w)
    ocp = OCP(A, B, C, D, T_lower, T_upper, u_lower, u_upper, a=a, b=b)
    outputCollection[key] = {"Z01_T":[], "Z02_T":[], "Fa_Pw_All":[], "Fa_Pw_HVAC":[], "Ext_T":[]}
    env.reset()
    luen, out = initEnv(env, A, B, C, D, sp_temp)

    for _ in range(num_steps):
        x0 = luen.getState()
        forecast = env.get_forecast(N_MPC)
        u = ocp.solve(x0, out["Fa_Pw_All"], np.array([sp_temp, sp_temp]), N_MPC, Q, forecast["Dry Bulb Temperature"], w=w, probType='linPW')
        out = env.step(arrayToInput(u))
        outputCollection = registerOutput(outputCollection, out, key)
        obs = outputToArray([out])
        luen.updateState(obs, u)


    pwSum, pwAvg = env.get_power_kpi(pw_type="All", start_ind=95)
    comAvg, totViolations = env.get_comfort_kpi(target=sp_temp, threshold=upper_tol, start_ind=95)
    kpiDict[key] = {"pwAvg":pwAvg, "comAvg":comAvg}


#############################################################
# Soft constrained
#############################################################

#ocp = OCP(A, B, C, D, sp_temp-upper_tol, sp_temp+upper_tol, u_lower, u_upper, a=a, b=b)
ocp = OCP(A, B, C, D, T_lower_pw_only, T_upper, u_lower_pw_only, u_upper, a=a, b=b)
outputCollection["soft"] = {"Z01_T":[], "Z02_T":[], "Fa_Pw_All":[], "Fa_Pw_HVAC":[], "Ext_T":[]}
env.reset()
luen, out = initEnv(env, A, B, C, D, sp_temp)

for _ in range(num_steps):
    x0 = luen.getState()
    forecast = env.get_forecast(N_MPC)
    u = ocp.solve(x0, out["Fa_Pw_All"], np.array([sp_temp, sp_temp]), N_MPC, Q, forecast["Dry Bulb Temperature"], probType='softPW')
    out = env.step(arrayToInput(u))
    outputCollection = registerOutput(outputCollection, out, "soft")
    obs = outputToArray([out])
    luen.updateState(obs, u)


pwSum, pwAvg = env.get_power_kpi(pw_type="All", start_ind=95)
comAvg, totViolations = env.get_comfort_kpi(target=sp_temp, threshold=upper_tol, start_ind=95)
kpiDict["soft"] = {"pwAvg":pwAvg, "comAvg":comAvg}

env.close()
#############################################################
# TempFan system ID
#############################################################

env = best.make('DatacenterTempFan-v0', simulation_days=100)

constsp1 = np.linspace(1.75,1.75,20)
constsp2 = np.linspace(4.5,4.5,20)
constsp3 = np.linspace(7,7,20)

excitationslistFan = [constsp1, constsp2, constsp3]

increasesp1 = np.linspace(17,20,20)
increasesp2 = np.linspace(14,17,20)
constsp1 = np.linspace(14, 14,20)
decreasesp1 = np.linspace(17,14,20)
constsp2 = np.linspace(20,20,20)
decreasesp2 = np.linspace(20,17,20)
constsp3 = np.linspace(18,18,20)
constsp4 = np.linspace(16,16,20)


excitationslistTemp = [increasesp1, increasesp2, decreasesp1, decreasesp2, constsp1, constsp2, constsp3, constsp4]

A, B, C, D = sysIDTempFan(env, excitationslistFan, excitationslistTemp, num=250, idType='ss')
a, b = sysIDTempFan(env, excitationslistFan, excitationslistTemp, num=100, idType='lin')


#############################################################
# TempFan mixed objective
#############################################################

for w in ws:
    key = "TempFan" + str(w)
    ocp = OCPTempFan(A, B, C, D, l_y=T_lower, u_y=T_upper, l_fan=1.75, u_fan=7, l_temp=13, u_temp=22, a=a, b=b)
    outputCollection[key] = {"Z01_T":[], "Z02_T":[], "Fa_Pw_All":[], "Fa_Pw_HVAC":[], "Ext_T":[]}
    env.reset()
    luen, out = initEnvTempFan(env, A, B, C, D)
    for _ in range(num_steps):
        x0 = luen.getState()
        forecast = env.get_forecast(N_MPC)
        u = ocp.solve(x0, out["Fa_Pw_All"], np.array([sp_temp, sp_temp]), N_MPC, Q, forecast["Dry Bulb Temperature"], w=w, probType='linPW')
        out = env.step(arrayToInputTempFan(u))
        outputCollection = registerOutput(outputCollection, out, key)
        obs = outputToArray([out])
        luen.updateState(obs, u)

    pwSum, pwAvg = env.get_power_kpi(pw_type="All", start_ind=95)
    comAvg, totViolations = env.get_comfort_kpi(target=sp_temp, threshold=upper_tol, start_ind=95)
    kpiDict[key] = {"pwAvg":pwAvg, "comAvg":comAvg}

#############################################################
# Plot Z01 temperatures
#############################################################

plt.plot(outputCollection["rbc"]["Z01_T"])
plt.plot(outputCollection["sp"]["Z01_T"])
plt.plot(outputCollection["mix0.95"]["Z01_T"])
plt.plot(outputCollection["mix0.85"]["Z01_T"])
plt.plot(outputCollection["mix0.75"]["Z01_T"])
plt.plot(outputCollection["soft"]["Z01_T"])
ax = plt.gca()
ax.legend(["RBC", "SP tracking", "Mixed obj 0.95", "Mixed obj 0.85", "Mixed obj 0.75", "Soft constr"])
plt.xlabel("Timestep")
plt.ylabel("Temperature Z01")
plt.show()

#############################################################
# Plot Z01 temperatures mixed obj
#############################################################

plt.plot(outputCollection["mix0.95"]["Z01_T"])
plt.plot(outputCollection["mix0.85"]["Z01_T"])
plt.plot(outputCollection["mix0.75"]["Z01_T"])
plt.plot(outputCollection["TempFan0.95"]["Z01_T"])
plt.plot(outputCollection["TempFan0.85"]["Z01_T"])
plt.plot(outputCollection["TempFan0.75"]["Z01_T"])
ax = plt.gca()
ax.legend(["MO 0.95", "MO 0.85", "MO 0.75", "TF 0.95", "TF 0.85", "TF 0.75"])
plt.xlabel("Timestep")
plt.ylabel("Temperature Z01")
plt.show()

#############################################################
# Plot power
#############################################################

plt.plot(outputCollection["rbc"]["Fa_Pw_All"])
plt.plot(outputCollection["sp"]["Fa_Pw_All"])
plt.plot(outputCollection["mix0.95"]["Fa_Pw_All"])
plt.plot(outputCollection["mix0.85"]["Fa_Pw_All"])
plt.plot(outputCollection["mix0.75"]["Fa_Pw_All"])
plt.plot(outputCollection["soft"]["Fa_Pw_All"])
ax = plt.gca()
ax.legend(["RBC", "SP tracking", "Mixed obj 0.95", "Mixed obj 0.85", "Mixed obj 0.75", "Soft constr"])
plt.xlabel("Timestep")
plt.ylabel("Power")
plt.show()


#############################################################
# Plot power
#############################################################

plt.plot(outputCollection["mix0.95"]["Fa_Pw_All"])
plt.plot(outputCollection["mix0.85"]["Fa_Pw_All"])
plt.plot(outputCollection["mix0.75"]["Fa_Pw_All"])
plt.plot(outputCollection["TempFan0.95"]["Fa_Pw_All"])
plt.plot(outputCollection["TempFan0.85"]["Fa_Pw_All"])
plt.plot(outputCollection["TempFan0.75"]["Fa_Pw_All"])
ax = plt.gca()
ax.legend(["MO 0.95", "MO 0.85", "MO 0.75", "TF 0.95", "TF 0.85", "TF 0.75"])
plt.xlabel("Timestep")
plt.ylabel("Power")
plt.show()

#############################################################
# Plot KPI
#############################################################

plt.plot(kpiDict["rbc"]["pwAvg"], kpiDict["rbc"]["comAvg"], 'o')
plt.plot(kpiDict["sp"]["pwAvg"], kpiDict["sp"]["comAvg"], 'o')
plt.plot(kpiDict["mix0.95"]["pwAvg"], kpiDict["mix0.95"]["comAvg"], 'o')
plt.plot(kpiDict["mix0.85"]["pwAvg"], kpiDict["mix0.85"]["comAvg"], 'o')
plt.plot(kpiDict["mix0.75"]["pwAvg"], kpiDict["mix0.75"]["comAvg"], 'o')
plt.plot(kpiDict["soft"]["pwAvg"], kpiDict["soft"]["comAvg"], 'o')
plt.plot(kpiDict["TempFan0.95"]["pwAvg"], kpiDict["TempFan0.95"]["comAvg"], 'o')
plt.plot(kpiDict["TempFan0.85"]["pwAvg"], kpiDict["TempFan0.85"]["comAvg"], 'o')
plt.plot(kpiDict["TempFan0.75"]["pwAvg"], kpiDict["TempFan0.75"]["comAvg"], 'o')
ax = plt.gca()
ax.legend(["RBC", "SP tracking", "MO 0.95", "MO 0.85", "MO 0.75", "Soft constr", "TF 0.95", "TF 0.85", "TF 0.75"])
plt.xlabel("Avg Power")
plt.ylabel("Avg Temp Deviation")
plt.show()


env.close()